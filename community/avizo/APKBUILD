# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=avizo
pkgver=1.2.1
pkgrel=0
pkgdesc="A neat notification daemon"
url="https://github.com/misterdanb/avizo"
arch="all"
license="GPL-3.0"
makedepends="
	gobject-introspection-dev
	gtk+3.0-dev
	gtk-layer-shell-dev
	meson
	vala
	"
subpackages="$pkgname-scripts::noarch"
source="https://github.com/misterdanb/avizo/archive/$pkgver/$pkgname-$pkgver.tar.gz
	replace-pactl-with-pamixer.patch
	unbundle-images.patch
	"
options="!check"  # no tests provided

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

scripts() {
	pkgdesc="Scripts to control sound volume and display backlight integrated with Avizo"
	depends="$pkgname brightnessctl pamixer"

	amove usr/bin/lightctl
	amove usr/bin/volumectl
}

sha512sums="
83880a176fe3a2e900a6a0bf3d6cd4bb725421017a8edd0e118d089bf480bcc3733738d56e1ce25bd55cdbc5a2bccacb3419233b8d27a2438b998703a962d247  avizo-1.2.1.tar.gz
10370a2fb58c3ec77f6e62b5cda0eb3db4b70992c125925cc5a52d64f535039ced3c9ea5b0830fe5c899ca12c07da757679c2ae232f62ebd77ec121fcbebad3c  replace-pactl-with-pamixer.patch
a42195eeb15e2ca2596c28904b6e23d6956a270939ffc972190756220e77be93a9006faacba0340d45dbb26e29fcdb622b89e8ea4bae341957262d6320eed24c  unbundle-images.patch
"
