# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=gallery-dl
pkgver=1.22.0
pkgrel=0
pkgdesc="CLI tool to download image galleries"
url="https://github.com/mikf/gallery-dl"
arch="noarch"
license="GPL-2.0-or-later"
depends="
	py3-requests
	python3
	"
makedepends="py3-setuptools"
checkdepends="py3-pytest yt-dlp"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikf/gallery-dl/archive/v$pkgver.tar.gz"

build() {
	python3 setup.py build

	make man completion
}

check() {
	make test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Install fish completion to the correct directory
	rm -r "$pkgdir"/usr/share/fish/vendor_completions.d
	install -Dm644 data/completion/gallery-dl.fish \
		-t "$pkgdir"/usr/share/fish/completions
}

sha512sums="
c621103562ad008327c1748b5ce0b49b4832511e1e2e12ee8656f49d1ae57abb133268ff6024a96a961de0949eceabd2b069441ceef9d30ea50e9bf52f846907  gallery-dl-1.22.0.tar.gz
"
