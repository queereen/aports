# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=lighthouse
pkgver=2.3.0
pkgrel=0
pkgdesc="Ethereum 2.0 Client"
url="https://lighthouse.sigmaprime.io/"
arch="x86_64 aarch64"  # limited by upstream
license="Apache-2.0"
makedepends="cargo clang-dev cmake openssl-dev protoc"
options="!check" # disable check as it takes too long
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sigp/lighthouse/archive/v$pkgver/lighthouse-$pkgver.tar.gz"

# secfixes:
#   2.2.0-r0:
#     - CVE-2022-0778

export OPENSSL_NO_VENDOR=true
export RUSTFLAGS="$RUSTFLAGS -L /usr/lib/"

build() {
	cargo build --release --locked
}

check() {
	cargo test --release --locked \
		--workspace \
			--exclude ef_tests \
			--exclude eth1 \
			--exclude genesis
}

package() {
	install -D -m755 "target/release/lighthouse" "$pkgdir/usr/bin/lighthouse"

	install -Dm 644 -t "$pkgdir/usr/share/doc/lighthouse" README.md
}

sha512sums="
d72cc73e8e21fcbc796fcf652b8b07b24ae88c7bc8600a2825a82ad24cc81c0f1d5f6c00d9b504baa558c4e01d701d06cddb08090c0070986cfba83004d96f4f  lighthouse-2.3.0.tar.gz
"
